# Docker Docksal Laravel

<p float="left">
  <img src="https://cdn.svgporn.com/logos/docker-icon.svg" height="60" title="docker">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/White_dot.svg/120px-White_dot.svg.png" width="40" title="dots">
  <img src="https://d33wubrfki0l68.cloudfront.net/96d4dedb7aa3fbf371d01d3356a97ec463b23e04/ca713/images/docksal-mark-color.svg" height="60" title="docksal">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/White_dot.svg/120px-White_dot.svg.png" width="40" title="dots">
  <img src="https://cdn.worldvectorlogo.com/logos/laravel-2.svg" height="60" title="laravel">
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/White_dot.svg/120px-White_dot.svg.png" width="40" title="dots">
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/4f/PhpMyAdmin_logo.svg" height="60" title="Phpmyadmin">
</p>

Docker + Docksal + Laravel + Phpmyadmin

## ⚙️ Requirements

- https://nodejs.org
- https://www.docker.com/
- https://docksal.io/

## 📍 Quick start

```bash
git clone https://github.com/Mgldvd/docksal-laravel.git .
fin init
npm run dev
```
NOTE: To reach the live reload `npm` must be executed outside the container, that's why it is not executed `fin npm run dev`

## 📍 Commands available

You can run just `fin` to see all commands available

## 📍 View project urls:

```bash
fin url
```
